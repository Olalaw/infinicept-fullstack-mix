<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DailyExceptionReportController;
use App\Http\Controllers\OutOfPatternActivityController;
use App\Http\Controllers\OverLimitProcessingController;
use App\Http\Controllers\MonthlyReportController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/get-first-transaction',[DailyExceptionReportController::class,'getFirstTransacion'])->name('getFirstTransaction');
Route::post('/daily_report',[DailyExceptionReportController::class,'store'])->name('store');
Route::post('/admin',[AdminController::class,'store'])->name('store');
Route::post('/watchlist',[MonthlyReportController::class,'saveWatchlist'])->name('storewatchlist');
Route::post('/matchlist',[MonthlyReportController::class,'saveMatchlist'])->name('storematchlist');
Route::get('/watchlist',[MonthlyReportController::class,'watchlist'])->name('watchlist');
Route::get('/getSubmerchants',[AdminController::class,'getSubmerchants'])->name('getSubmerchants');
Route::get('/matchlist',[MonthlyReportController::class,'getMatchList'])->name('matchlist');
Route::get('/watchlist',[MonthlyReportController::class,'getWatchList'])->name('watchlist');
Route::get('/test',[DailyExceptionReportController::class,'test'])->name('test');
Route::get('/daily_report',[DailyExceptionReportController::class,'index'])->name('index');
Route::get('/top_processing',[MonthlyReportController::class,'TopProcessingSubmerchants'])->name('topProcessing');
Route::get('/ol_report',[OverLimitProcessingController::class,'index'])->name('index');
Route::get('/submerchant/getReport/{id}',[OverLimitProcessingController::class,'getReport'])->name('report');
Route::get('/submerchant/getWeeklyReport/{id}',[OverLimitProcessingController::class,'getWeeklyReport'])->name('report');
Route::get('/getUser',[DailyExceptionReportController::class,'getUser'])->name('getUser');
Route::get('/ticket-greater',[DailyExceptionReportController::class,'ticketGreater'])->name('ticketGreater');
Route::get('/getTransactions/{submerchant}',[DailyExceptionReportController::class,'getTransactions'])->name('ticketGreater');
Route::get('/ticket-over-maximum',[DailyExceptionReportController::class,'ticketOverMaximum'])->name('ticketOverMaximum');
Route::get('/daily-deposit-over-maximum',[DailyExceptionReportController::class,'dailyDepositOverMax'])->name('dailyDepositOverMax');
Route::get('/multiple-auth-over-card',[DailyExceptionReportController::class,'multipleAuthOverMax'])->name('multipleAuthOverMax');
Route::get('/daily-transaction-card',[DailyExceptionReportController::class,'dailyTransactionNumber'])->name('dailyTransactionNumber');
Route::get('/declined-auth-over-card',[DailyExceptionReportController::class,'declinedDailyAuthNumber'])->name('declinedDailyAuthNumber');
Route::get('/same-dollar',[DailyExceptionReportController::class,'sameDollar'])->name('sameDollar');
Route::get('/zero-dollar',[DailyExceptionReportController::class,'zeroDollar'])->name('zeroDollar');
Route::get('/abnormal-transaction-hours',[DailyExceptionReportController::class,'abnormalTransactionHours'])->name('abnormalTransactionHours');
Route::get('/credit-over-max',[DailyExceptionReportController::class,'creditOverMax'])->name('creditOverMax');
Route::get('/credit-returns-over',[DailyExceptionReportController::class,'creditReturnsOver'])->name('creditReturnsOver');
route::get('/High_Percentage_of_or_Large_Chargebacks', [DailyExceptionReportController::class, 'high_Percentage_of_or_Large_Chargebacks'])->name('high_Percentage_of_or_Large_Chargebacks');
Route::get('/High_Percentage_of_or_Large_Retrievals', [DailyExceptionReportController::class, 'high_Percentage_of_or_Large_Retrievals'])->name('high_Percentage_of_or_Large_Retrievals');
Route::get('/Credits_Issued_Subsequent_to_a_Chargeback', [DailyExceptionReportController::class, 'credits_Issued_Subsequent_to_a_Chargeback'])->name('credits_Issued_Subsequent_to_a_Chargeback');
Route::get('/Fallback_Transactions', [DailyExceptionReportController::class, 'fallback_Transactions'])->name('fallback_Transactions');
Route::get('/Collection_Balance', [DailyExceptionReportController::class, 'collection_Balance'])->name('collection_Balance');
route::get('/New_Submerchant', [DailyExceptionReportController::class, 'new_Submerchant'])->name('new_Submerchant');
Route::get('/Excessive_Number_of_Forced_Authorizations', [DailyExceptionReportController::class, 'excessive_Number_of_Forced_Authorizations'])->name('excessive_Number_of_Forced_Authorizations');
Route::get('/Excessive_Foreign_Card_Percentage', [DailyExceptionReportController::class, 'excessive_Foreign_Card_Percentage'])->name('excessive_Foreign_Card_Percentage');
route::get('/Monitoring_and_or_Tracking_of_the_Monthly_Approved_Limit', [DailyExceptionReportController::class, 'monitoring_and_or_Tracking_of_the_Monthly_Approved_Limit'])->name('monitoring_and_or_Tracking_of_the_Monthly_Approved_Limit');
Route::get('/Submerchant_Excessive_Processing_Review', [DailyExceptionReportController::class, 'submerchant_Excessive_Processing_Review'])->name('submerchant_Excessive_Processing_Review');
route::get('/Exceptions_Based_on_Approved_Parameters', [OutOfPatternActivityController::class, 'exceptions_Based_on_Approved_Parameters'])->name('exceptions_Based_on_Approved_Parameters');
Route::get('/Exceptions_Based_on_Average_Activity', [OutOfPatternActivityController::class, 'exceptions_Based_on_Average_Activity'])->name('exceptions_Based_on_Average_Activity');
route::get('/Top_Processing_Submerchants', [MonthlyReportController::class, 'top_Processing_Submerchants'])->name('top_Processing_Submerchants');
Route::get('/Restricted_Business_Type_Review', [MonthlyReportController::class, 'restricted_Business_Type_Review'])->name('restricted_Business_Type_Review');
Route::get('/Decreasing_Volume_Submerchants', [MonthlyReportController::class, 'decreasing_Volume_Submerchants'])->name('decreasing_Volume_Submerchants');
Route::view('/metronic/{path}', 'home')->where('path', '([A-z\d\-\/_.]+)?')->middleware('auth');
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
