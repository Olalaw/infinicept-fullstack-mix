import React from 'react'
import SVG from 'react-inlinesvg'
import {toAbsoluteUrl} from '../AssetHelpers'
type Props = {
  className?: string
  path: string
  svgClassName?: string
}

const KTSVG: React.FC<Props> = ({className = '', path, svgClassName = 'mh-50px'}) => {
  return (
    <span className={`svg-icon ${className}`}>
      <SVG src={path} className={svgClassName} />
    </span>
  )
}

export {KTSVG}
{/* <td className='text-dark fw-bold text-hover-primary fs-6'>
{getPercentage(reports[index].total,reports[index].refundVolume)   +'('}
<small> {getPercentage(getPercentage(reports[index].total,reports[index.refundVolume]),getPercentage(reports[index+1].total,reports[index+1].refundVolume))}% </small>
    {getPercentage(reports[index].total,reports[index].refundVolume)  > getPercentage(reports[index+1].total,reports[index+1].refundVolume)  ? (
        <KTSVG path='/media/icons/duotune/arrows/arr066.svg' className='svg-icon-success ms-1' />
                    //  /media/icons/duotune/arrows/arr001.svg
    ) : (
        getPercentage(reports[index].total,reports[index].refundVolume)  < getPercentage(reports[index+1].total,reports[index+1].refundVolume)  && (
        <KTSVG path='/media/icons/duotune/arrows/arr065.svg' className='svg-icon-danger ms-1' />
        )
    )}
    {')'}
</td> */}
