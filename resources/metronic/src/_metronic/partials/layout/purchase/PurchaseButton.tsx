import React, {FC} from 'react'

const PurchaseButton: FC = () => (
  <a
    href={'https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469'}
    className='engage-purchase-link btn btn-flex h-35px bg-body btn-color-gray-700 btn-active-color-gray-900 px-5 shadow-sm rounded-top-0'
  >
    Buy Now
  </a>
)

export {PurchaseButton}
