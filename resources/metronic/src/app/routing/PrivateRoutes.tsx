import {lazy, FC, Suspense} from 'react'
import {Route, Routes, Navigate} from 'react-router-dom'
import {MasterLayout} from '../../_metronic/layout/MasterLayout'
import TopBarProgress from 'react-topbar-progress-indicator'
import {DashboardWrapper} from '../pages/dashboard/DashboardWrapper'
import {MenuTestPage} from '../pages/MenuTestPage'
import {getCSSVariableValue} from '../../_metronic/assets/ts/_utils'
import {WithChildren} from '../../_metronic/helpers'
import BuilderPageWrapper from '../pages/layout-builder/BuilderPageWrapper'
import DailyReportPageWrapper from '../modules/dailyReport/DailyReportPage'
import OLReportPage from '../modules/overLimit/OLReportPage'
import SubmerchantTransactionPage from '../modules/dailyReport/SubmerchantTransactionPage'
import SubmerchantReportPage from '../modules/overLimit/SubmerchantReportPage'
import SubmerchantWeeklyReportPage from '../modules/overLimit/SubmerchantWeeklyReportPage'
import TopProcessingPage from '../modules/TopProcessingPage/TopProcessingPage'
import WatchlistPage from '../modules/WatchlistPage/WatchlistPage'
import MatchlistPage from '../modules/MatchlistPage/MatchlistPage'
import AdminPage from '../modules/Admin/AdminPage'

const PrivateRoutes = () => {
  const ProfilePage = lazy(() => import('../modules/profile/ProfilePage'))
  const WizardsPage = lazy(() => import('../modules/wizards/WizardsPage'))
  const AccountPage = lazy(() => import('../modules/accounts/AccountPage'))
  const WidgetsPage = lazy(() => import('../modules/widgets/WidgetsPage'))
  const ChatPage = lazy(() => import('../modules/apps/chat/ChatPage'))
  const UsersPage = lazy(() => import('../modules/apps/user-management/UsersPage'))


  return (
    <Routes>
      <Route element={<MasterLayout />}>
        {/* Redirect to Dashboard after success login/registartion */}
        <Route path='auth/*' element={<Navigate to='/dashboard' />} />
        {/* Pages */}
        <Route path='dashboard' element={<DashboardWrapper />} />
        <Route path='builder' element={<BuilderPageWrapper />} />
        <Route path='menu-test' element={<MenuTestPage />} />
        <Route path='daily-report' element={<DailyReportPageWrapper />} />
        <Route path='ol-report' element={<OLReportPage />} />
        <Route path='top-processing' element={<TopProcessingPage />} />
        <Route path='matchlist' element={<MatchlistPage />} />
        <Route path='watchlist' element={<WatchlistPage />} />
        <Route path="submerchant/weekly/:subMerchant" element={<SubmerchantWeeklyReportPage />} />
        <Route path="admin" element={<AdminPage />} />
        <Route path="ol-report/submerchant/report/:subMerchant" element={<SubmerchantReportPage />} />
        <Route path="daily-report/submerchant/:subMerchant" element={<SubmerchantTransactionPage />} />
        {/* Lazy Modules */}
        <Route
          path='crafted/pages/profile/*'
          element={
            <SuspensedView>
              <ProfilePage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/pages/wizards/*'
          element={
            <SuspensedView>
              <WizardsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/widgets/*'
          element={
            <SuspensedView>
              <WidgetsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/account/*'
          element={
            <SuspensedView>
              <AccountPage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/chat/*'
          element={
            <SuspensedView>
              <ChatPage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/user-management/*'
          element={
            <SuspensedView>
              <UsersPage />
            </SuspensedView>
          }
        />
        {/* Page Not Found */}
        <Route path='*' element={<Navigate to='/error/404' />} />
      </Route>
    </Routes>
  )
}

const SuspensedView: FC<WithChildren> = ({children}) => {
  const baseColor = getCSSVariableValue('--bs-primary')
  TopBarProgress.config({
    barColors: {
      '0': baseColor,
    },
    barThickness: 1,
    shadowBlur: 5,
  })
  return <Suspense fallback={<TopBarProgress />}>{children}</Suspense>
}

export {PrivateRoutes}
