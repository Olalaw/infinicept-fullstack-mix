import React, {FC} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import { Link } from 'react-router-dom'
import { KTSVG } from '../../../_metronic/helpers'
import { useEffect,useState } from 'react'
import { useParams } from 'react-router-dom'


// const DailyReportPage: React.FC = () => (


// )

const TopProcessingPage: FC = () => {


    const [reports, setReports] = useState([]);


    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await fetch('/top_processing/' ); // Adjust the URL accordingly
            if (response.ok) {
              const data = await response.json();
              setReports(data);
            } else {
              console.error('Failed to fetch data');
            }
          } catch (error) {
            console.error('An error occurred during the fetch', error);
          }
        };

        fetchData();
      }, []);
  return (
    <>
      <PageTitle breadcrumbs={[]}>Top Processing Submerchants</PageTitle>
      <div className='row flex space-x-15'>
   <div className={`card col-6`}>
      {/* begin::Header */}
      <div className='card-header border-0 pt-5'>
        <h3 className='card-title align-items-start flex-column'>
          <span className='card-label fw-bold fs-3 mb-1'>Submerchants with highest Sales volume</span>
          <span className='text-muted mt-1 fw-semibold fs-7'>{}</span>
        </h3>

      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className='card-body py-3 row'>
        {/* begin::Table container */}
        <div className='table-responsive'>
          {/* begin::Table */}
          <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
            {/* begin::Table head */}
            <thead>
              <tr className='fw-bold text-muted'>

                <th className='min-w-50px'>SID</th>
                <th className='min-w-100px'>Name</th>
                <th className='min-w-50px'>Total Volume</th>
                <th className='min-w-30px'>Total Count</th>
                <th className='min-w-50px'>Action</th>

              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
            {reports.length > 0 && (
            reports[0].map((report, index) => (

            <>
            <tr key={index}>
              <td className='text-dark fw-bold text-hover-primary fs-6' onClick={()=>handleSelect(report)}>{report.id}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.name}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.total_amount}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.count}</td>
              <td className='text-dark fw-bold text-hover-danger fs-6'><a>Review</a></td>

            </tr>
            </>

          ))
          )}

            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>

        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
    <div className={`card col-5 ms-12`}>
      {/* begin::Header */}
      <div className='card-header border-0 pt-5'>
        <h3 className='card-title align-items-start flex-column'>
          <span className='card-label fw-bold fs-3 mb-1'>Submerchants with highest Refunds</span>
          <span className='text-muted mt-1 fw-semibold fs-7'>{}</span>
        </h3>

      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className='card-body py-3 row'>
        {/* begin::Table container */}
        <div className='table-responsive'>
          {/* begin::Table */}
          <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
            {/* begin::Table head */}
            <thead>
              <tr className='fw-bold text-muted'>

                <th className='min-w-50px'>SID</th>
                <th className='min-w-100px'>Name</th>
                <th className='min-w-50px'>Total Volume</th>
                <th className='min-w-30px'>Total Count</th>
                <th className='min-w-50px'>Action</th>

              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
            {reports.length > 0 && (
            reports[1].map((report, index) => (

            <>
            <tr key={index}>
              <td className='text-dark fw-bold text-hover-primary fs-6' onClick={()=>handleSelect(report)}>{report.id}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.name}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.total_amount}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.count}</td>
              <td className='text-dark fw-bold text-hover-danger fs-6'><a>Review</a></td>

            </tr>
            </>

          ))
          )}

            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>

        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
    </div>
    <div className={`card col-8 mt-8`}>
      {/* begin::Header */}
      <div className='card-header border-0 pt-5'>
        <h3 className='card-title align-items-start flex-column'>
          <span className='card-label fw-bold fs-3 mb-1'>Submerchants with highest Chargebacks</span>
          <span className='text-muted mt-1 fw-semibold fs-7'>{}</span>
        </h3>

      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className='card-body py-3 row'>
        {/* begin::Table container */}
        <div className='table-responsive'>
          {/* begin::Table */}
          <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
            {/* begin::Table head */}
            <thead>
              <tr className='fw-bold text-muted'>

                <th className='min-w-50px'>SID</th>
                <th className='min-w-100px'>Name</th>
                <th className='min-w-50px'>Total Volume</th>
                <th className='min-w-30px'>Total Count</th>
                <th className='min-w-50px'>Action</th>

              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
            {reports.length > 0 && (
            reports[2].map((report, index) => (

            <>
            <tr key={index}>
              <td className='text-dark fw-bold text-hover-primary fs-6' onClick={()=>handleSelect(report)}>{report.id}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.name}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.total_amount}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{report.count}</td>
              <td className='fw-bold text-success fs-6 text-hover-primary'><a href={'/metronic/submerchant/weekly/'+report.id}>Review</a></td>

            </tr>
            </>

          ))
          )}

            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>

        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
    </>
  )
}

export default TopProcessingPage
