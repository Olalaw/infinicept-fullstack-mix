import React, {FC} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import { Link } from 'react-router-dom'
import { KTSVG } from '../../../_metronic/helpers'
import { useEffect,useState } from 'react'
import { useParams } from 'react-router-dom'


// const DailyReportPage: React.FC = () => (


// )

const SubmerchantTransactionPage: FC = () => {
    const { subMerchant } = useParams();

    const [transactions, setTransactions] = useState([]);
    const [currentSubmerchant, setCurerentSubmerchant] = useState('');
    const [issue, setIssue] = useState('');

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await fetch('/getTransactions/'+ subMerchant ); // Adjust the URL accordingly
            if (response.ok) {
              const data = await response.json();
              setTransactions(data[0]);
              setIssue(data[1]);
              setCurerentSubmerchant(data[0][0].submerchant.name)
            } else {
              console.error('Failed to fetch data');
            }
          } catch (error) {
            console.error('An error occurred during the fetch', error);
          }
        };

        fetchData();
      }, []);
  return (
    <>
      <PageTitle breadcrumbs={[]}>{currentSubmerchant}</PageTitle>
      <>
   <div className={`card`}>
      {/* begin::Header */}
      <div className='card-header border-0 pt-5'>
        <h3 className='card-title align-items-start flex-column'>
          <span className='card-label fw-bold fs-3 mb-1'>All Transactions For {currentSubmerchant}</span>
          <span className='text-muted mt-1 fw-semibold fs-7'>{}</span>
        </h3>

      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className='card-body py-3 row'>
        {/* begin::Table container */}
        <div className='table-responsive'>
          {/* begin::Table */}
          <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
            {/* begin::Table head */}
            <thead>
              <tr className='fw-bold text-muted'>

                <th className='min-w-100px'>TID</th>
                <th className='min-w-100px'>Amount</th>
                <th className='min-w-150px'>Submerchant</th>
                <th className='min-w-100px'>Type</th>
                <th className='min-w-100px'>Issue</th>

              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
            {transactions.map((transaction, index) => (
            <tr key={index}>
              <td className='text-dark fw-bold text-hover-primary fs-6' onClick={()=>handleSelect(transaction)}>{transaction.number}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{transaction.amount}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{transaction.submerchant.name}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{transaction.transaction_type.name}</td>
              <td className='text-dark fw-bold text-hover-danger fs-6'>{issue}</td>

            </tr>
          ))}

            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>

        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
    </>
    </>
  )
}

export default SubmerchantTransactionPage
