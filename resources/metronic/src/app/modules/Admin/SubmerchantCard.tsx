// SubmerchantCard.tsx
import React, { useState } from 'react';
import Submerchant from './Submerchant'; // Import your Submerchant type or interface

interface SubmerchantCardProps {
  submerchant: Submerchant;
  propertyToEdit: keyof Submerchant;
  onUpdate: (updatedSubmerchant: Submerchant) => void;
}

const SubmerchantCard: React.FC<SubmerchantCardProps> = ({ submerchant, propertyToEdit, onUpdate }) => {
  const [value, setValue] = useState(submerchant[propertyToEdit]);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    onUpdate({ ...submerchant, [propertyToEdit]: event.target.value });
  };

  return (

    <div className='col-3 card mx-5'>
      <div className='card-head'>
        <h4 className='text-center fw-bold text-primary my-6'>{`Set ${String(propertyToEdit)}`}</h4>
      </div>
      <div className='card-body'>
        <div className='col-10 offset-1'>
          <input
            className='form-control'
            onChange={handleInputChange}
            value={String(value)}
          />
        </div>
      </div>
    </div>

  );
};

export default SubmerchantCard;
