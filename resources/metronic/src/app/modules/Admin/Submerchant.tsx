interface Submerchant {
    id:string;
    name: string;
    processing_limit: number;
    card_network_graduation_limits: string;
    approved_volume: number;
    isActive: boolean;
    maxAmount: number;
    dailyDepositMaxAmount:number;
    collection_balance: string; // Adjust the type accordingly
    batch_thresholds: string; // Adjust the type accordingly
    address: string;
    email: string;
    foreignCardCount: number;
    grossSalesCount: number;
    foreignCardMax: number;
    phone: string;
    refundPercentageMax: number;
    refundCountPercentageMax: number;
    cardAuthNumberLimit: number;
    monthly_processing_limit: number;
    cardTransactionNumberLimit: number;
    cardDeclinedNumberLimit: number;
    creditRefundMax: number;
    last_activity: string;
  }
  export default Submerchant
