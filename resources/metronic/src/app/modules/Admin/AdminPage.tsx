import React, {FC} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import { Link } from 'react-router-dom'
import { KTSVG } from '../../../_metronic/helpers'
import { useEffect,useState } from 'react'
import { useParams } from 'react-router-dom'
import Submerchant from './Submerchant'; // Import your Submerchant type or interface
import SubmerchantCard from './SubmerchantCard'
import axios from 'axios'
import Swal from 'sweetalert2'



// const DailyReportPage: React.FC = () => (


// )
// interface Submerchant {
//     id:string;
//     name: string;
//     processing_limit: number;
//     card_network_graduation_limits: string;
//     approved_volume: number;
//     isActive: boolean;
//     maxAmount: number;
//     dailyDepositMaxAmount:number;
//     collection_balance: string; // Adjust the type accordingly
//     batch_thresholds: string; // Adjust the type accordingly
//     address: string;
//     email: string;
//     foreignCardCount: number;
//     grossSalesCount: number;
//     foreignCardMax: number;
//     phone: string;
//     refundPercentageMax: number;
//     refundCountPercentageMax: number;
//     cardAuthNumberLimit: number;
//     monthly_processing_limit: number;
//     cardTransactionNumberLimit: number;
//     cardDeclinedNumberLimit: number;
//     creditRefundMax: number;
//     last_activity: string;
//   }
const AdminPage: FC = () => {
    const [submerchants, setSubmerchants] = useState<Submerchant[]>([]);
    const [currentSubmerchant, setCurrentSubmerchant] =useState<Submerchant | null>(null);

    function selectSubmerchant(event:any){

        const selectedSubmerchantId = event.target.value;

        const selectedSubmerchant = submerchants.find(submerchant => submerchant.id == selectedSubmerchantId);

        if (selectedSubmerchant) {
            // setCurrentSubmerchant(selectedSubmerchant);

            setCurrentSubmerchant(selectedSubmerchant);
          } else {
            console.error(`Submerchant with id ${selectedSubmerchantId} not found.`);
          }
    }

    const handleSubmit = async (e:any)=>{

        // console.log(inputElement.files[0].name);
        try {

            // Make a POST request to the Laravel backend
            const response = await axios.post('/admin',currentSubmerchant,
          {headers: {
            'Content-Type': 'multipart/form-data', // Set Content-Type to handle file uploads
          }
      });
          console.log(response.data)
          Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: 'Parameters changed succesfully',
            }).then(() => {
              // Reload the page after the SweetAlert is closed

              window.location.reload();
            });
          } catch (error) {
            // Handle error
            console.error('Error creating resolution:', error);
          }
    }

    const handleUpdateSubmerchant = (updatedSubmerchant: Submerchant) => {
        setSubmerchants((prevSubmerchants) =>
          prevSubmerchants.map((submerchant) =>
            submerchant.id === updatedSubmerchant.id ? updatedSubmerchant : submerchant
          )
        );
        setCurrentSubmerchant(updatedSubmerchant)
      };
    const handleInputChange = (event:any) => {
        setCurrentSubmerchant((prevSubmerchant) => {
            if (prevSubmerchant) {
              return {
                ...prevSubmerchant,
                dailyDepositMaxAmount: event.target.value,
              };
            }
            return null; // Return null if prevSubmerchant is null
          });
        }

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await fetch('/getSubmerchants');
            if (response.ok) {
                const data = await response.json();
                setSubmerchants(data);
            } else {
              console.error('Failed to fetch data');
            }
          } catch (error) {
            console.error('An error occurred during the fetch', error);
          }
        };

        fetchData();
      }, []);
      var renderSubmerchantCards = () => {

        if (!currentSubmerchant) {
          return <h3 className='mt-3'>No submerchant selected</h3>;
        }


        // Dynamically generate SubmerchantCard components based on the selected submerchant
        return Object.keys(currentSubmerchant).map((property) => (
          <SubmerchantCard
          key={`${property}-${(currentSubmerchant as any)[property]}`}
            submerchant={currentSubmerchant}
            propertyToEdit={property as keyof Submerchant}
            onUpdate={handleUpdateSubmerchant}
          />
        ));

      };

  return (
    <>
      <PageTitle breadcrumbs={[]}>Set Controls</PageTitle>
      <>
      <div className='col-10 g-5 g-xl-8'>
        <select className='form-select'
        name="submerchant"
        onChange={selectSubmerchant}>
            <option>Select Submerchant</option>
            {submerchants.map((submerchant)=>
            <option key={submerchant.id} value={submerchant.id}>
                {submerchant.name + " " + submerchant.email}
            </option>
            )}
        </select>
      </div>

        <div className='row mt-5  g-5'>

        {renderSubmerchantCards()}

         </div>

        <div className='col-10  mt-3 p-3'>
                <button className='btn btn-success' onClick={handleSubmit}>Change</button>
        </div>
     </>
    </>
  )
}

export default AdminPage
