function downloadCSV() {
    // Get the table
    var table = document.getElementById('table') as HTMLTableElement;

    // Prepare CSV content
    var csvContent = "data:text/csv;charset=utf-8,";

    // Add header row
    var headerRow = table.rows[0];
    for (var j = 0; j < headerRow.cells.length; j++) {
      csvContent += '"' + headerRow.cells[j].innerText + '"' + (j < headerRow.cells.length - 1 ? ',' : '\n');
    }

    // Add data rows
    for (var i = 1; i < table.rows.length; i++) {
      var dataRow = table.rows[i];
      for (var j = 0; j < dataRow.cells.length; j++) {
        csvContent += '"' + dataRow.cells[j].innerText + '"' + (j < dataRow.cells.length - 1 ? ',' : '\n');
      }
    }

    // Create a download link
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "report.csv");
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  export default downloadCSV
