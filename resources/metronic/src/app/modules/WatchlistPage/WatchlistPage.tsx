import React, {FC} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import { Link } from 'react-router-dom'
import { KTSVG } from '../../../_metronic/helpers'
import { useEffect,useState } from 'react'
import { useParams } from 'react-router-dom'


// const DailyReportPage: React.FC = () => (


// )
interface Submerchant {
    id: string;
    name: string;
    email:string;
    phone: string;
    // Add any other fields based on your requirements
}
interface watchType {
    id: string;
    name: string;
    // Add any other fields based on your requirements
}
interface watch {
    number: string;
    amount: number;
    watch_type_id: number;
    submerchant_id: string;
    card_id: string;
    batch: string;
    batch_amount: number;
    submerchant:Submerchant;
    collection_amount: number;
    terminal_number: string;
    cardholder: string;
    status: boolean;
    manual_entry: boolean;
    swiped_mag_stripe: boolean;
    time_created: string;
    watch_type:watchType
    // Add any other fields based on your requirements
}
const WatchlistPage: FC = () => {
    const { subMerchant } = useParams();

    const [watch, setwatch] = useState<Submerchant[] | []>([]);


    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await fetch('/watchlist'); // Adjust the URL accordingly
            if (response.ok) {
              const data = await response.json();
              setwatch(data);

            } else {
              console.error('Failed to fetch data');
            }
          } catch (error) {
            console.error('An error occurred during the fetch', error);
          }
        };

        fetchData();
      }, []);
  return (
    <>
      <PageTitle breadcrumbs={[]}>Submerchants Added To Watch List</PageTitle>
      <>
   <div className={`card`}>
      {/* begin::Header */}
      <div className='card-header border-0 pt-5'>
        <h3 className='card-title align-items-start flex-column'>
          <span className='card-label fw-bold fs-3 mb-1'>All Submerchants</span>
          <span className='text-muted mt-1 fw-semibold fs-7'>{}</span>
        </h3>

      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className='card-body py-3 row'>
        {/* begin::Table container */}
        <div className='table-responsive'>
          {/* begin::Table */}
          <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
            {/* begin::Table head */}
            <thead>
              <tr className='fw-bold text-muted'>

                <th className='min-w-100px'>Submerchant</th>
                <th className='min-w-100px'>Address</th>
                <th className='min-w-150px'>Email</th>
                <th className='min-w-100px'>Phone</th>


              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
            {watch ?
            watch.map((watch:any, index:any) => (
            <tr key={index}>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{watch.submerchant.name}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{watch.email}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{watch.address}</td>
              <td className='text-dark fw-bold text-hover-primary fs-6'>{watch.phone}</td>


            </tr>
          )):<p>not found</p>
        }

            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>

        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
    </>
    </>
  )
}

export default WatchlistPage
