<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Daily Report Form</title>
</head>
<body>

  <form action="/daily_report" method="post" enctype="multipart/form-data">
    @csrf <!-- Laravel CSRF token -->

    <label for="number">Number:</label>
    <input type="text" id="number" name="number" required>

    <label for="submerchant_id">Submerchant ID:</label>
    <input type="text" id="submerchant_id" name="submerchant_id" required>

    <label for="issue">Issue:</label>
    <textarea id="issue" name="issue" required></textarea>

    <label for="image">Select Image:</label>
    <input type="file" id="image" name="image" accept="image/*" required>

    <button type="submit">Submit Report</button>
  </form>

</body>
</html>
