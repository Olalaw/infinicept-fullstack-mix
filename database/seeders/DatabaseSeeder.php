<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\AdminThresholds;
use Illuminate\Database\Seeder;
use App\Models\TransactionType;
use App\Models\Transaction;
use App\Models\Roles;
use App\Models\Submerchant;
use App\Models\Card;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        Submerchant::factory(3)->create();
        AdminThresholds::factory()->create();
        Card::factory(10)->create();
        User::factory(2)->create();
        Transaction::factory(75)->create();
        // TransactionType::factory(3)->create();
        Roles::factory()->create([
            'name' => 'Risk Analyst'
        ]);
        Roles::factory()->create([
            'name' => 'Manager'
        ]);
        Roles::factory()->create([
            'name' => 'Company Officer'
        ]);
        TransactionType::factory()->create([
            'name' => 'Debit'
        ]);
        TransactionType::factory()->create([
            'name' => 'Credit'
        ]);
        TransactionType::factory()->create([
            'name' => 'ChargeBack'
        ]);
        \App\Models\User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
            'password' => '12345',
        ]);
        \App\Models\User::factory()->create([
            'name' => 'Sayo Law',
            'email' => 'sayo@example.com',
            'password' => '12345',
        ]);

    }
}
