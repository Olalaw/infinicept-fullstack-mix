<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Card>
 */
class CardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'cardholder' => $this->faker->name(),
            'cardholder_no' => $this->faker->creditCardNumber,
            'exp_date' =>  $this->faker->creditCardExpirationDate,
            'card_type' =>  $this->faker->numberBetween(1,2),
            'country' =>  "Canada",
            'submerchant_id' =>  $this->faker->numberBetween(1,3),

        ];
    }
}

