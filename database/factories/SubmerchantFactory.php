<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Submerchant>
 */
class SubmerchantFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return  [
            'name' => $this->faker->company,
            'processing_limit' => $this->faker->numberBetween(100, 5000),
            'monthly_processing_limit' => $this->faker->randomFloat(2,8000,10000),
            'card_network_graduation_limits' => $this->faker->numberBetween(500, 2000),
            'approved_volume' => $this->faker->randomFloat(2, 10000, 50000),
            'collection_balance' => $this->faker->randomFloat(2, 1000, 10000),
            'ticketsMaxAmount' => $this->faker->randomFloat(2, 1000, 5000),
            'foreignCardMax' => $this->faker->numberBetween(10, 90),
            'dailyDepositMaxAmount' => $this->faker->randomFloat(2, 5000,10000),
            'isActive' => $this->faker->boolean(),
            'batch_thresholds' => $this->faker->numberBetween(100, 1000),
            'address' => $this->faker->address,
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->phoneNumber,
            'foreignCardCount' =>  $this->faker->numberBetween(1, 10),
            'chargeback_threshold' =>  $this->faker->numberBetween(30, 60),
            'refund_threshold' =>  $this->faker->numberBetween(30, 60),
            'weekly_chargeback_threshold' =>  $this->faker->numberBetween(30, 60),
            'weekly_refund_threshold' =>  $this->faker->numberBetween(30, 60),
            'grossSalesCount' =>  $this->faker->numberBetween(5, 15),
            'cardAuthNumberLimit' => $this->faker->numberBetween(5, 10),
            'cardTransactionNumberLimit' => $this->faker->numberBetween(5, 10),
            'cardDeclinedNumberLimit' => $this->faker->numberBetween(3, 4),
            'creditRefundmax' => $this->faker->randomFloat(2, 5000, 7000),
            'refundPercentageMax'=> $this->faker->numberBetween(50, 60),
            'refundCountPercentageMax'=> $this->faker->numberBetween(50, 60),
            'last_activity' => $this->faker->dateTimeBetween('-6 months', 'now'),
            'created_at' => $this->faker->dateTimeBetween('-1 year', 'now'),
            'updated_at' => $this->faker->dateTimeBetween('-1 year', 'now'),
        ];
    }
}
