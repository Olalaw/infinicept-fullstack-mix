<?php

namespace Database\Factories;
// namespace App\Models;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\models\TransactionType;
use App\models\Submerchant;
use App\Models\Card;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $submerchant = Submerchant::inRandomOrder()->first();
        return [
            'number' => $this->faker->randomNumber(),
            'Amount' => $this->faker->randomFloat(2, 100, 500),
            'transaction_type_id' =>  $this->faker->numberBetween(1,3),
            'submerchant_id' => $submerchant->id,
            'batch' => $this->faker->numberBetween(1,2),
            'collection_balance' => $this->faker->randomFloat(2, 0, 100),
            'batch_amount' => $this->faker->randomFloat(2, 1000, 5000),
            'terminal_number' => $this->faker->numberBetween(1,1000),
            'card_id' => $submerchant->cards->random()->id,
            'status' => 1,
            'manual_entry' => $this->faker->numberBetween(0,1),
            'swiped_mag_stripe' => $this->faker->numberBetween(0,1),

            'created_at' => $this->faker->dateTimeBetween('-5 months', 'now'),
            'updated_at' => $this->faker->dateTimeBetween('-1 year', 'now'),


        ];
    }
}
