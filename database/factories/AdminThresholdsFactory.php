<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AdminThresholds>
 */
class AdminThresholdsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            //

            'refund_threshold' =>  $this->faker->numberBetween(80, 90),
            'chargeback_threshold' =>  $this->faker->numberBetween(80, 90),
            'weekly_refund_threshold' =>  $this->faker->numberBetween(80, 90),
            'weekly_chargeback_threshold' =>  $this->faker->numberBetween(80, 90),
        ];
    }
}
