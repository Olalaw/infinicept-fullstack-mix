<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\models\TransactionType;
use App\models\Submerchant;
use App\models\Card;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->decimal('amount', 10, 2);
            $table->foreignIdFor(TransactionType::class);
            $table->foreignIdFor(Submerchant::class);
            $table->foreignIdFor(Card::class);
            $table->integer('status');
            $table->integer('batch');
            $table->decimal('batch_amount',10,2);
            $table->decimal('collection_balance',10,2);
            $table->integer('terminal_number');
            $table->integer('manual_entry');
            $table->integer('swiped_mag_stripe');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
