<?php

use App\Models\Submerchant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('over_limit_incidents', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Submerchant::class);

            $table->string('issue');
            $table->boolean('status');
            $table->decimal('volume');
            $table->integer('count');
            $table->decimal('avergae');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('over_limit_incidents');
    }
};
