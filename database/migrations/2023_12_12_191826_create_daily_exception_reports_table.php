<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // $table->engine = “InnoDB”;
        Schema::create('daily_exception_reports', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('submerchant_id')->unsigned();
            $table->string('type');
            $table->text('description');
            $table->string('priority');
            $table->timestamps();
        });
       
       
    }

    public function down()
    {
        Schema::dropIfExists('daily_exception_reports');
    }

    /**
     * Reverse the migrations.
     */
  
};
