<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('monthly_reports', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('submerchant_id');
            $table->decimal('cad_volume', 10, 2);
            $table->integer('chargeback_volume');
            $table->integer('refund_volume');
            $table->string('mcc');
            $table->string('vertical');
            $table->string('restricted_business_type');
            $table->string('decreasing_volume');
            $table->timestamps();

            
        });
     
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('monthly_reports');
    }
};
