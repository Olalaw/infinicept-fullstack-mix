<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\models\TransactionType;
use App\models\Transaction;
use App\models\Submerchant;
use App\models\Card;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Transaction::class);

            $table->string('issue');
            $table->boolean('status');
            $table->string('image')->nullable();
            $table->string('notes')->nullable();
            $table->string('analyst')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('incidents');
    }
};
