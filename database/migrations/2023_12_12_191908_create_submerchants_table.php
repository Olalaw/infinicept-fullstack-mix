<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // $table->engine = “InnoDB”;
        Schema::create('submerchants', function (Blueprint $table) {

            $table->id();
            $table->string('name');
            $table->integer('processing_limit');
            $table->decimal('monthly_processing_limit',10,2);
            $table->integer('card_network_graduation_limits');
            $table->boolean('isActive');
            $table->decimal('approved_volume', 10, 2);
            $table->decimal('dailyDepositMaxAmount', 10, 2);
            $table->decimal('ticketsMaxAmount', 10, 2);
            $table->decimal('collection_balance', 10, 2);
            $table->integer('batch_thresholds');
            $table->string('address');
            $table->string('email')->unique();
            $table->string('phone');
            $table->integer('cardAuthNumberLimit');
            $table->integer('foreignCardMax');
            $table->integer('foreignCardCount');
            $table->integer('chargeback_threshold');
            $table->integer('refund_threshold');
            $table->integer('weekly_chargeback_threshold');
            $table->integer('weekly_refund_threshold');
            $table->integer('grossSalesCount');
            $table->integer('cardTransactionNumberLimit');
            $table->integer('refundPercentageMax');
            $table->integer('refundCountPercentageMax');
            $table->integer('cardDeclinedNumberLimit');
            $table->decimal('creditRefundMax', 10, 2);
            $table->dateTime('last_activity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('submerchants');
    }
};
