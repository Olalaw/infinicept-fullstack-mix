<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admin_thresholds', function (Blueprint $table) {
            $table->id();
            $table->integer('chargeback_threshold');
            $table->integer('refund_threshold');
            $table->integer('weekly_chargeback_threshold');
            $table->integer('weekly_refund_threshold');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('admin_thresholds');
    }
};
