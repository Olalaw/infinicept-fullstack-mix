<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthlyReport extends Model
{
    protected $fillable = ['submerchant_id', 'cad_volume', 'chargeback_volume', 'refund_volume', 'mcc', 'vertical', 'restricted_business_type', 'decreasing_volume'];

    public function submerchant()
    {
        return $this->belongsTo(Submerchant::class);
    }
}
