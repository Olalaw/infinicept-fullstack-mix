<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Incidents extends Model
{
    use HasFactory;
    protected $fillable = [
        'transaction_id',
        'issue',
        'image',
        'notes',
        'analyst',
        'status'
    ];
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
    public function checklist()
    {
        return $this->hasMany(Checklist::class);
    }
}
