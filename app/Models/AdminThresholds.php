<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminThresholds extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'chargeback_threshold',
        'refund_threshold',
        'weekly_chargeback_threshold',
        'weekly_refund_threshold',
        'status'
    ];
}
