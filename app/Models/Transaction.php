<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'number',
        'amount',
        'transaction_type_id',
        'submerchant_id',
        'card_id',
        'batch',
        'batch_amount',
        'collection_amount',
        'terminal_number',
        'cardholder',
        'status',
        'manual_entry',
        'swiped_mag_stripe',
        'time_created'
    ];
    public function transactionType()
    {
        return $this->belongsTo(TransactionType::class);
    }
    public function incidents()
    {
        return $this->hasMany(Incidents::class);
    }
    public function submerchant()
    {
        return $this->belongsTo(Submerchant::class);
    }
    public function card()
    {
        return $this->belongsTo(Card::class);
    }
}
