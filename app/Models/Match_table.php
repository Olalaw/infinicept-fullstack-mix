<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match_table extends Model
{
    use HasFactory;
    protected $fillable = [
        'submerchant_id',
        'name',
        'address',
        'email',
        'phone',
    ];
  
    public function submerchant()
    {
        return $this->belongsTo(Submerchant::class);
    }
}
