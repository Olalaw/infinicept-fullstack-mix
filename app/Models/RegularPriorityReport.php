<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegularPriorityReport extends Model
{
    protected $fillable = ['exception_report_id'];

    public function dailyExceptionReport()
    {
        return $this->belongsTo(DailyExceptionReport::class);
    }
}
