<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OverLimitIncidents extends Model
{
    use HasFactory;
    protected $fillable = [
        'submerchant_id',
        'issue',
        'image',
        'volume',
        'count',
        'avergae',
        'status'
    ];
    public function submerchant()
    {
        return $this->belongsTo(Submerchant::class);
    }
    public function checklist()
    {
        return $this->hasMany(Checklist::class);
    }
}

