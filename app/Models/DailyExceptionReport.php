<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyExceptionReport extends Model
{
    protected $fillable = ['submerchant_id', 'type', 'description', 'priority'];

    public function submerchant()
    {
        return $this->belongsTo(Submerchant::class);
    }

    public function highPriorityReport()
    {
        return $this->hasOne(HighPriorityReport::class);
    }

    public function regularPriorityReport()
    {
        return $this->hasOne(RegularPriorityReport::class);
    }
}
