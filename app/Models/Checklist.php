<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    use HasFactory;
    protected $fillable = [
        'incidents_id',
        'over_limit_incidents_id',
        'name',
        'status',
    ];
    public function incidents()
    {
        return $this->belongsTo(Incidents::class);
    }
}
