<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submerchant extends Model
{
   use hasFactory;
    protected $fillable = [
        'name',
        'processing_limit',
        'card_network_graduation_limits',
        'approved_volume',
        'isActive',
        'maxAmount',
        'collection_balance',
        'batch_thresholds',
        'address',
        'email',
        'foreignCardCount',
        'grossSalesCount',
        'foreignCardMax',
        'dailyDepositMaxAmount',
        'chargeback_threshold',
        'refund_threshold',
        'weekly_chargeback_threshold',
        'weekly_refund_threshold',
        'phone',
        'refundPercentageMax',
        'refundCountPercentageMax',
        'cardAuthNumberLimit',
        'monthly_processing_limit',
        'cardTransactionNumberLimit',
        'cardDeclinedNumberLimit',
        'creditRefundMax',
        'last_activity'
    ];

    public function dailyExceptionReports()
    {
        return $this->hasMany(DailyExceptionReport::class);
    }

    public function monthlyReports()
    {
        return $this->hasMany(MonthlyReport::class);
    }
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
    public function cards()
    {
        return $this->hasMany(Card::class);
    }
}
