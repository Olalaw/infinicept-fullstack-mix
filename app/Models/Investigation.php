<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investigation extends Model
{
    protected $fillable = ['report_id', 'investigator_id', 'status', 'result'];

    public function dailyExceptionReport()
    {
        return $this->belongsTo(DailyExceptionReport::class, 'report_id');
    }
}
