<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;
    protected $fillable = [
        'cardholder',
        'cardholder_no',
        'exp_date',
        'card_type',
        'submerchant_id',
    ];

    public function submerchant()
    {
        return $this->belongsTo(Submerchant::class);
    }
}
