<?php

namespace App\Http\Controllers;

use App\Models\Submerchant;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function getSubmerchants(){
        return Submerchant::all();
    }
    public function store(Request $request){
        $incident = Submerchant::find($request->id);

        $incident->update(
            $request->all()
        );
        return $incident;
    }
}
