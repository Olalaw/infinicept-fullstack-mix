<?php

namespace App\Http\Controllers;
use App\Models\Match_table;
use Illuminate\Support\Facades\Storage;
use App\Models\Checklist;
use App\Models\Incidents;
use Illuminate\Http\Request;
use App\models\DailyExceptionReport;
use App\Models\Transaction;
use App\Models\Submerchant;
use App\Models\Resolution;
use Auth;
use Carbon\Carbon;
class DailyExceptionReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $start = Carbon::now()->subWeeks(2)->startOfMonth();
        // dd($start);
        $end = Carbon::now()->subWeeks(2)->endOfMonth();
        $dailyExceptionReports = Transaction::with('transactionType','submerchant','submerchant.cards')
        ->whereBetween('created_at',[$start,$end])
        ->get();
        // dd($dailyExceptionReports);
        $count = 0;
        $transactions = [];

        foreach($dailyExceptionReports as $transaction){
            $match = $this->checkForMatch($transaction->submerchant_id,$transaction->submerchant->email,
            $transaction->submerchant->phone,$transaction->submerchant->address,$transaction->submerchant->cards);
            // dd($match);
            if(!$match){

            $this->getFirstTransaction($transaction, $transactions);
            $this->New_Submerchant($transaction);
                if($transaction->transaction_type_id==1){
          $this->ticketGreater($transaction, $transactions);
         $this->ticketOverMaximum($transaction, $transactions);
        }
         $this->dailyDepositOverMax($transaction);
         $this->multipleAuthOverMax($transaction);
         $this->sameDollar($transaction);
         $this->zeroDollar($transaction);
         $this->Excessive_Foreign_Card_Percentage($transaction);
         $this->Fallback_Transactions($transaction);
         $this->Collection_Balance($transaction);
         if($transaction->status==0){
         $this->declinedDailyAuthNumber($transaction);
        }
        else{
            $this->dailyTransactionNumber($transaction);
        }
        $this->abnormalTransactionHours($transaction);
        if($transaction->transaction_type_id!=1){
        $this->creditOverMax($transaction);
        $this->High_Percentage_of_or_Large_Chargebacks($transaction);
        $this->ExcessiveRefund($transaction);
        $this->ExcessiveRefundCount($transaction);
        $this->creditReturnsOver($transaction);
        $this->CreditsWithoutanOffsettingSale($transaction);
        $this->Credits_Issued_Subsequent_to_a_Chargeback($transaction);
        }
        if($transaction->manual_entry == 1){
            $this->Excessive_Number_of_Forced_Authorizations($transaction);
        }
    }
    else{
        // $count++;
    }
    }
    // dd($count);
       $incidents = Incidents::with('transaction.transactionType','transaction.submerchant','checklist')->get();
        // dd($incidents);
        return $incidents;
        // return view('daily_exception_reports.index', compact('dailyExceptionReports'));
    }
    public function checkForMatch($id,$email,$phone,$address,$card_number){
        $fraud = Match_table::with('submerchant', 'submerchant.cards')
        ->where(function ($query) use ($id, $email, $phone, $card_number,$address) {
            $query->where('submerchant_id', $id)
                ->orWhere('email', $email)
                ->orWhere('address', $address)
                ->orWhere('phone', $phone);
                // ->orWhereHas('submerchant.cards', function ($subquery) use ($card_number) {
                //     $subquery->where('cardholder_no', $card_number->cardholder_no);
                // });
        })
        ->first();
        if($fraud){

            return true;
        }
        return false;
    }
    public function addToIncident($id,$issue,$checklist){

        $incident = Incidents::firstOrCreate(
            [
                'transaction_id' => $id,
                'issue' => $issue,
            ],
            [
                'status' => false,
                'image' => null,
                // Other attributes you want to set for a new record
            ]
        );

       foreach($checklist as $check){
        Checklist::firstOrcreate([
            'incidents_id' => $incident->id,
            // 'over_limit_incidents_id' => $incident->id,
            'name' => $check,
        ],

            [

            'status' => 0,

        ]);
    }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            // 'id' => 'required',
            'notes'=>'required'
        ]);

        if($request->file('image')){

        $imageData = $request->file('image');
             $filename = $imageData->store('images', 's3');

        // $filename = time() . '.' . $imageData->getClientOriginalExtension();
        // $request->image->move(public_path('images/uploads'), $filename);
        }
        else{
            $filename = null;
        }

        // Create a new Resolution entry
       $incident = Incidents::find($request->id);

       $incident->update([
        'image' => Storage::disk('s3')->url($filename),
        'notes' => $request->notes,
        'analyst' => Auth::user()->name,
        'status' => true
       ]);
       $incident->checklist()->update([
        'status' => true
       ]);


        return $incident;
    }

    public function test(){
        return view('test');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
    public function getFirstTransaction($transaction,$transactions){

        // $transactions= Transaction::with('transactionType','submerchant')
        // ->whereHas('submerchant', function ($query){
        //     $query->where('isActive', false);
        // })->get();

        if($transaction->submerchant->isActive==false){

            $checklist=[
                "Has the new submerchant activity been compared to typical processing activity for similar types of submerchants as well as the application and original projections."];


            $issue="First Transaction";
            $this->addToIncident($transaction->id,$issue,$checklist);
            // $mytransaction = new Transaction($transaction->toArray());
            // // dd($mytransaction);
            // $mytransaction->submerchant = $transaction->submerchant;
            // $mytransaction->transaction_type = $transaction->transactionType;

            // $mytransaction['issues'] = 'First Transaction';
            // $result = Resolution::where('number',$mytransaction->number)->where('issue',$mytransaction->issues)->first();
            // if($result){
            //     $mytransaction->status = 2;
            // }
            // $mytransaction['checklist'] = $checklist;
            // $transactions[]=$mytransaction;

            return;
        }

       return;
    }
    public function ticketGreater($transaction,$transactions){
        // $transactions = Transaction::with('transactionType','submerchant')
        // ->where('amount','>',500)->get();


        if($transaction->amount >= 1300){

            $checklist=[
                "Has the account been reviewed to establish an appropriate range of transaction amounts and an average ticket amount.",
                "Is the average ticket normal or has the submerchant been interviewed to ensure appropriate account parameters that align with the originally approved submerchant activities" ];
                $issue="Ticket Greater 1300";
                $this->addToIncident($transaction->id,$issue,$checklist);
            //     $mytransaction = new Transaction($transaction->load('submerchant', 'transactionType')->toArray());

            // $mytransaction->issues = 'Ticket Over 1300 dollars';
            // $result = Resolution::where('number',$mytransaction->number)->where('issue',$mytransaction->issues)->first();
            // if($result){
            //     $mytransaction->status = 2;
            // }
            // $mytransaction->submerchant = $transaction->submerchant;
            // $mytransaction->transaction_type = $transaction->transactionType;
            // $mytransaction->checklist = $checklist;
            // $transactions[]=$mytransaction;
            return;
        }
        return ;
    }
    public function ticketOverMaximum($transaction, $transactions){
        // $transactions = Transaction::join('submerchants', 'transactions.submerchant_id', '=', 'submerchants.id')
        // ->whereColumn('transactions.amount','>=','submerchants.ticketsMaxAmount')
        // ->select('transactions.*')
        // ->get();

        if($transaction->amount >= $transaction->submerchant->ticketsMaxAmount){
            $checklist=[
                "Has the approved average ticket amount along with prior transaction dollar amounts been used to determine an average range for transactions."];
                $issue="Ticket Over Maximum";
                $this->addToIncident($transaction->id,$issue,$checklist);

            //     $mytransaction = new Transaction($transaction->load('submerchant', 'transactionType')->toArray());

            // $mytransaction->issues = 'Ticket Over Maximum';
            // $result = Resolution::where('number',$mytransaction->number)->where('issue',$mytransaction->issues)->first();
            // if($result){
            //     $mytransaction->status = 2;
            // }
            // $mytransaction->submerchant = $transaction->submerchant;
            // $mytransaction->transaction_type = $transaction->transactionType;
            // $mytransaction->checklist = $checklist;
            // $transactions[]=$mytransaction;
            return;
        }
        return;
    }
    public function dailyDepositOverMax($transaction){

        // $transactions = Transaction::join('submerchants', 'transactions.submerchant_id', '=', 'submerchants.id')
        // ->where('transactions.transaction_type_id','=','1')
        // ->whereColumn('transactions.amount','>=','submerchants.dailyDepositMaxAmount')
        // ->select('transactions.*')
        // ->get();
        $maxAmount = $transaction->submerchant->dailyDepositMaxAmount;
        $incidents = Transaction::join('submerchants', 'transactions.submerchant_id', '=', 'submerchants.id')
    ->where('transactions.transaction_type_id', 1)
    ->where('submerchants.id', $transaction->submerchant_id)
    ->where('transactions.created_at', Carbon::now())
    ->groupBy('transactions.id')
    ->havingRaw('SUM(transactions.amount) > ?', [$maxAmount])
    ->select('transactions.*')
    ->get();
    if($incidents->isNotEmpty()){
        $checklist=["Has the monthly approved volume been used to establish a daily deposit range and project a trend of that month’s processing volume."];
        $issue="Daily Deposit Over Maximum";
        $this->addToIncident($transaction->id,$issue,$checklist);
    return;
}
    else{
        return;
    }

    //    return view('layout.getFirstTransaction')->with('transactions', $transactions);
    }
    public function multipleAuthOverMax($transaction){
    //     $groupByColumns = [

    //         'transactions.card_id',
    //         'submerchants.cardAuthNumberLimit',
    //     ];
    //     $cardNumbersQuery = Transaction::join('cards', 'transactions.card_id', '=', 'cards.id')
    //     // ->where('status','=',2)
    //     ->join('submerchants','transactions.submerchant_id','=','submerchants.id')
    //     ->groupBy( $groupByColumns)
    //     ->havingRaw('COUNT(*) > submerchants.cardAuthNumberLimit')
    //     ->select('transactions.card_id');

    //     $transactions = Transaction::with('card','submerchant')
    //     ->whereIn('card_id', $cardNumbersQuery)
    //     ->get();

    //     // dd($transactions);
    $maxAmount = $transaction->submerchant->cardAuthNumberLimit;
    $incidents = Transaction::join('cards', 'transactions.card_id', '=', 'cards.id')
                ->where('cards.id', $transaction->card_id)
                ->where('transactions.created_at',Carbon::now())
                ->count();

if($incidents > $maxAmount){

    $checklist=["Have you reviewied the transaction records to determine an average ticket, and possibly a transaction range, which can be used to profile the activity of the cardholder in question",
"Has previous processing been reviewed to determine a possible pattern of duplicated charges per card number.",
"Is the number of sales per cardholder verified to determine if authorizations have been obtained that were not processed with a sale",
];
$issue="Multiple Authorization On the same Card";
$this->addToIncident($transaction->id,$issue,$checklist);

return;
}
else{
    return;
}

    //    return view('layout.cardMethods')->with('transactions', $transactions);
    }
    public function declinedDailyAuthNumber($transaction){
    //     $groupByColumns = [

    //         'transactions.card_id',
    //         'submerchants.cardDeclinedNumberLimit',
    //     ];
    //     $cardNumbersQuery = Transaction::join('cards', 'transactions.card_id', '=', 'cards.id')
    //     ->where('status','=','1')
    //     ->join('submerchants','transactions.submerchant_id','=','submerchants.id')
    //     ->groupBy( $groupByColumns)
    //     ->havingRaw('COUNT(*) > submerchants.cardDeclinedNumberLimit')
    //     ->select('transactions.card_id')->get();
    //     // dd($cardNumbersQuery);

    //     $transactions = Transaction::with('card','submerchant')
    //     ->whereIn('card_id', $cardNumbersQuery)
    //     ->where('status','=','1')
    //     ->get();

    //     // dd($transactions);

    //    return view('layout.cardMethods')->with('transactions', $transactions);

    $maxAmount = $transaction->submerchant->cardDeclinedNumberLimit;
    $incidents = Transaction::join('submerchants', 'transactions.submerchant_id', '=', 'submerchants.id')

->where('transactions.status', 0)
->where('submerchants.id', $transaction->submerchant_id)
->where('transactions.created_at', Carbon::now())
->count();

if($incidents>$maxAmount){
    $checklist=["RapidCents will determine if there are any patterns to the declined authorization attempts (i.e. same card, descending dollar amounts, same time of day, same bin range, etc.).",
"RapidCents will contact the submerchant to review the authorization process to ensure their business model has not changed and ensure that they are not experiencing a technical problem"
];
$issue="Multiple Declined Authorizations";
$this->addToIncident($transaction->id,$issue,$checklist);

return;
}
else{
    return;
}
    }
    public function dailyTransactionNumber($transaction){
    //     $groupByColumns = [

    //         'transactions.card_id',
    //         'submerchants.cardTransactionNumberLimit',
    //     ];
    //     $cardNumbersQuery = Transaction::join('cards', 'transactions.card_id', '=', 'cards.id')
    //     ->where('status','=',2)
    //     ->join('submerchants','transactions.submerchant_id','=','submerchants.id')
    //     ->groupBy( $groupByColumns)
    //     ->havingRaw('COUNT(*) > submerchants.cardTransactionNumberLimit')
    //     ->select('transactions.card_id');

    //     $transactions = Transaction::with('card','submerchant')
    //     ->whereIn('card_id', $cardNumbersQuery)
    //     ->where('status','=','2')
    //     ->get();

    //     // dd($transactions);

    //    return view('layout.cardMethods')->with('transactions', $transactions);
    $maxAmount = $transaction->submerchant->cardTransactionNumberLimit;
    $incidents = Transaction::join('cards', 'transactions.card_id', '=', 'cards.id')
                ->where('cards.id', $transaction->card_id)
                ->where('transactions.created_at', Carbon::now())
                ->where('transactions.status', 1)
                ->count();

if($incidents > $maxAmount){

    $checklist=["Did you review the transaction records to determine an average ticket, and possibly a transaction range",
"Has Previous processing been reviewed to determine a possible pattern of duplicated charges per card number.  ",
"Has the submerchant been instructed not to split a transaction to evade an authorization limit. The submerchant will be contacted to determine why multiple transactions per card appear.",
];
$issue="Multiple Transactions On Same Card";
$this->addToIncident($transaction->id,$issue,$checklist);

return;
}
else{
    return;
}


    }

public function sameDollar($transaction){

//     $duplicateTransactions = Transaction::join('cards', 'transactions.card_id', '=', 'cards.id')
//     ->join('submerchants', 'transactions.submerchant_id', '=', 'submerchants.id')
//     ->select('transactions.*')
//     ->groupBy('cards.cardholder_no', 'transactions.amount', 'submerchants.id')
//     ->havingRaw('COUNT(*) > 1')
//     ->get();

//    return view('layout.cardMethods')->with('transactions', $duplicateTransactions);
// $maxAmount = $transaction->submerchant->cardTransactionNumberLimit;
$incidents = Transaction::with('submerchant')
            // ->where('cards.id', $transaction->card_id)
            ->where('transactions.amount', $transaction->amount)
            ->where('transactions.created_at', Carbon::now())
            ->where('transactions.batch',$transaction->batch)
            ->count();

if($incidents > 1){

    $checklist=["RapidCents will review the records to determine an average ticket and possibly a transaction range which can be used to profile the dollar amount in question.",
    "Previous processing will also be reviewed to determine a possible pattern of transactions with a repeating dollar amount."
    ];
    $issue="Same Amount in Batch";
    $this->addToIncident($transaction->id,$issue,$checklist);

return;
}
else{
return;
}

}
public function zeroDollar($transaction){
//     $groupByColumns = [

//         'transactions.card_id',
//         'submerchants.cardTransactionNumberLimit',
//     ];
//     $duplicateTransactions = Transaction::join('cards', 'transactions.card_id', '=', 'cards.id')
//     ->join('submerchants', 'transactions.submerchant_id', '=', 'submerchants.id')
//    ->where('transactions.amount','=',0)
//    ->get();

//    return view('layout.cardMethods')->with('transactions', $duplicateTransactions);


if($transaction->amount == 0.00){

    $checklist=["RapidCents will review the records to determine if this batch contains credits and debits to the same or different cards.",
    "RapidCents will conduct research as defined in Section 12 below."
    ];
    $issue="Zero amount in batch";
        $this->addToIncident($transaction->id,$issue,$checklist);

return;
}
else{
return;
}
}

public function abnormalTransactionHours($transaction){

//     $transactions = Transaction::whereTime('created_at', '>=', '02:00:00')
//     ->whereTime('created_at', '<=', '05:00:00')
//     ->get();

//    return view('layout.cardMethods')->with('transactions', $transactions);

if($transaction->created_at->format('H:i:s') >= '02:00:00' && $transaction->created_at->format('H:i:s') <= '05:00:00'){

    $checklist=["Has previous processing been reviewed to determine a possible pattern of processing at abnormal times.",

    ];
    $issue="Abnormal transaction time";
        $this->addToIncident($transaction->id,$issue,$checklist);

return;
}
else{
return;
}
}
public function creditOverMax($transaction){
//     $transactions = Transaction::select('transactions.submerchant_id')
//     ->where('transactions.transaction_type_id','=',3)
//     ->selectRaw('SUM(amount) as totalAmount')
//     ->join('submerchants', 'transactions.submerchant_id', '=', 'submerchants.id')
//     ->groupBy('transactions.submerchant_id', 'submerchants.creditRefundMax')
//     ->havingRaw('SUM(amount) > submerchants.creditRefundMax')
//     ->join('cards', 'transactions.card_id', '=', 'cards.id')
//     ->select('transactions.*','cards.cardholder')
//     ->get();

//     // dd($transactions);

//    return view('layout.cardMethods')->with('transactions', $transactions);
$maxAmount  = $transaction->submerchant->creditRefundMax;

$incidents = Transaction::with('submerchant')
            ->where('transactions.card_id', $transaction->card_id)
            ->where('transactions.transaction_type_id', 3)
            // ->where('transactions.batch',$transaction->batch)
            ->sum('amount');


if($incidents > $maxAmount){

    $checklist=["RapidCents will review the records to determine an average ticket, and possibly a transaction range",
    "Previous processing will be reviewed to determine a possible pattern of duplicated credits or PRAs per card number."
    ];
    $issue="Mulitple Refunds to the same card";
    $this->addToIncident($transaction->id,$issue,$checklist);

return;
}
else{
return;
}

}
public function ExcessiveRefund($transaction){
    $maxAmount  = $transaction->submerchant->refundPercentageMax;

    $sumOfCredits = Transaction::with('submerchant')
                ->where('transactions.submerchant_id', $transaction->submerchant_id)
                ->where('transactions.transaction_type_id', 3)
                // ->where('transactions.batch',$transaction->batch)
                ->sum('amount');

    $sumOfSales = Transaction::with('submerchant')
    ->where('transactions.submerchant_id', $transaction->submerchant_id)
                ->where('transactions.transaction_type_id','!=',3)
                // ->where('transactions.batch',$transaction->batch)
                ->sum('amount');
                // dd($sumOfCredits);

    $incidents = ($sumOfCredits/$sumOfSales) * 100 ;



    if($incidents > $maxAmount){

        $checklist=["RapidCents will verify through the submerchant processing history that there were prior sales for the same or a greater dollar amount on the same card."
        ];
        $issue="Excessive Refund";
        $this->addToIncident($transaction->id,$issue,$checklist);

    return;
    }
    else{
    return;
    }
}

public function ExcessiveRefundCount($transaction){
    $maxAmount  = $transaction->submerchant->refundCountPercentageCountMax;

    $sumOfCredits = Transaction::with('submerchant')
                ->where('transactions.submerchant_id', $transaction->submerchant_id)
                ->where('transactions.transaction_type_id', 3)
                // ->where('transactions.batch',$transaction->batch)
                ->count();

    $sumOfSales = Transaction::with('submerchant')
    ->where('transactions.submerchant_id', $transaction->submerchant_id)
                ->where('transactions.transaction_type_id','!=',3)
                // ->where('transactions.batch',$transaction->batch)
                ->count();
                // dd($sumOfCredits);

    $incidents = ($sumOfCredits/$sumOfSales) * 100 ;


    if($incidents > $maxAmount){

        $checklist=[" RapidCents will review the submerchant’s processing and approved parameters to determine if the number of returns or PRAs is excessive for that submerchant.",
        "RapidCents will verify that there were prior sales for the same or greater dollar amounts on the same cards.
        "
        ];
        $issue="Excessive Refund Count";
        $this->addToIncident($transaction->id,$issue,$checklist);

    return;
    }
    else{
    return;
    }
}
public function creditReturnsOver($transaction){
    // $transactions =  Transaction::with('submerchant','card')
    // ->where('transactions.transaction_type_id','=',3)
    // ->where('transactions.amount','>','300')
    // ->get();


    // dd($transactions);
    $maxAmount  = 300;
    $checklist=["RapidCents will review the account and approved parameters to establish current processing parameters.",
    "RapidCents will ensure an offsetting sale (same card number and greater or equal value than the credit PRA) has occurred.
    ",
    "If the credit activity is in excess of the approved parameters, the submerchant will be contacted."
    ];
    $issue = "Credit is Over $300";
    if($transaction->amount>=300){
        $this->addToIncident($transaction->id,$issue,$checklist);
    }

    return;


//    return view('layout.cardMethods')->with('transactions', $transactions);
}
public function CreditsWithoutanOffsettingSale($transaction){
      $transactions =  Transaction::with('submerchant','card')
    ->where('transactions.card_id',$transaction->card_id)
    ->where('transactions.transaction_type_id','=',1)
    ->where('transactions.amount',$transaction->amount)
    ->first();
    $checklist=["RapidCents will review the account for the offsetting sales (same card number and greater or equal value than the credit or PRA) and approved parameters to establish current processing parameters.",
    "RapidCents will ensure an offsetting sale (same card number and greater or equal value than the credit PRA) has occurred.
    ",
    "If the credit activity is in excess of the approved parameters, the submerchant will be contacted."
    ];
    $issue = "Refund without an offsetting sale";
    if(!$transactions){
        $this->addToIncident($transaction->id,$issue,$checklist);
    }

    return;
}
public function getTransactions($subMerchant){
    $transactions= Transaction::with('transactionType','submerchant')
    ->where('submerchant_id', $subMerchant)->get();

   return [$transactions,'All Transactions'];
}
public function New_Submerchant($transaction){
    $checklist=["be compared to average portfolio processing for the submerchant’s assigned MCC or business type.",
    "RapidCents will review the prior active months’ processing or approved parameters to determine if the current monetary activity is like that of previous or approved activity.",
    "If the credit activity is in excess of the approved parameters, the submerchant will be contacted."
    ];
    $issue = "New Submerchant or First Deposit ";
    $transactions = Submerchant::where('id',$transaction->submerchant_id)->first();
    $creation_date = $transactions->created_at;
   $last_activity = Transaction::where('submerchant_id', $transaction->submerchant_id)
   ->latest('created_at')
   ->first();
   $today = Carbon::now()->subMonths(3);
   if($today<=$creation_date || $today >= $last_activity){
    $this->addToIncident($transaction->id,$issue,$checklist);

   }




    // $transactions = $transactions->whereHas('submerchant', function ($query){
    //     $query->where('last_activity', '<=', Carbon::now()->subMonths(3)->toDateString())
    //     ->orWhere('isActive','=','0');
    //    })->get();


    // return view('layout.cardMethods')->with('transactions', $transactions);


}
public function getUser(){
    return auth()->user()->load('role');
}
public function Excessive_Number_of_Forced_Authorizations($transaction){
    $transactions = Transaction::where('submerchant_id',$transaction->submerchant_id)
    ->where('manual_entry','=',1)
    ->count();


    $issue="Excessive Forced Authorizations";
    // return view('layout.cardMethods')->with('transactions', $transactions);
    $checklist=["the submerchant must be interviewed to determine why there is a need to process sales by means of the forced authorization method.",
    "Such submerchants must be reviewed in accordance with Enhanced Due Diligence controls contained herein as part of the determination",
    "RapidCents should review at each instance if such controls are in place to accurately assess all forced auths and prevent fraudulent transactions from being submitted",
    "The account should be placed on the Watch List for daily monitoring to ensure continuing transaction review until the problem is corrected.",
    "Supporting transaction documentation must be requested from the submerchant on all ticket-only transactions."
    ];

   if($transactions > 2){
    $this->addToIncident($transaction->id,$issue,$checklist);

   }
}
public function Excessive_Foreign_Card_Percentage($transaction){
    $foreign = Transaction::with('card','submerchant')
    ->where('submerchant_id',$transaction->submerchant_id)
    ->whereHas('card', function ($query) {
        $query->where('country', '!=', 'Canada');
    })
    ->where('transaction_type_id',1)
    ->sum('amount');

    $sales = Transaction::where('submerchant_id',$transaction->submerchant_id)
    ->where('transaction_type_id',1)
    ->sum('amount');
    if($foreign>0){
    $percentage = ($foreign/$sales)*100;
    }
    else{
        $percentage = 0;
    }


    $issue="Excessive Foreign Card Use";
    // return view('layout.cardMethods')->with('transactions', $transactions);
    $checklist=[
        "Review the submerchant’s processing, location, product, and approved parameters to determine if the number of foreign cards is excessive for that submerchant.
        ",
        "If the foreign card volume is determined to be excessive, the submerchant must be contacted to explain the increase in foreign card processing.
        "
    ];

   if($percentage > $transaction->submerchant->foreignCardMax){

    $this->addToIncident($transaction->id,$issue,$checklist);

   }


}
public function High_Percentage_of_or_Large_Chargebacks($transaction){
    $chargebacks = Transaction::where('submerchant_id',$transaction->submerchant_id)
    ->where('transaction_type_id',3)
    ->whereBetween('created_at',[Carbon::now()->startOfMonth(),Carbon::now()])
    ->sum('amount');

    $sales = Transaction::where('submerchant_id',$transaction->submerchant_id)
    ->where('transaction_type_id',1)
    ->whereBetween('created_at',[Carbon::now()->startOfMonth(),Carbon::now()])
    ->sum('amount');

    if($chargebacks > 0 && $sales>0){

    $percentage = ($chargebacks/$sales)*100;
    }
    else{
       if($sales==0){
        $percentage = 100;
       }
       else{
        $percentage = 0;
       }

    }


    $issue="High Percentage of or Large Chargebacks ";
    // return view('layout.cardMethods')->with('transactions', $transactions);
    $checklist=[
       "Review the chargeback reports to obtain the previous and current month’s chargeback ratios.
       ",
       "If the chargeback count and percentages exceed the established maximums and/or indicate an escalating trend, the submerchant must be contacted
       ",
       "The specific reasons for each chargeback should also be reviewed.
       ",
       "Depending upon the severity of the chargebacks, a Chargeback Action Plan that lists specific steps to reduce the excessive percentage of chargebacks may also be required.
       "
    ];

   if($percentage > 30){

    $this->addToIncident($transaction->id,$issue,$checklist);
   }
}

public function Credits_Issued_Subsequent_to_a_Chargeback($transaction){
    if($transaction->transaction_type_id==3){
    $transactions =  Transaction::with('submerchant','card')
    ->where('transactions.card_id',$transaction->card_id)
    ->where('transactions.transaction_type_id','=',2)
    ->where('transactions.amount',$transaction->amount)
    ->first();
    $checklist=[
        "the situation will be investigated to determine the circumstances and to identify whether a second presentment has been made."
    ];
    $issue = "Credits Issued Subsequent to a Chargeback  ";

    if($transactions){

        $this->addToIncident($transaction->id,$issue,$checklist);
    }
}
    return;
}
public function Fallback_Transactions($transaction){



    $issue="Fallback Transactions";
    // return view('layout.cardMethods')->with('transactions', $transactions);
    $checklist=[
        "Such transactions pose a risk and will be investigated"
    ];

   if($transaction->swiped_mag_stripe==1){

    $this->addToIncident($transaction->id,$issue,$checklist);
   }
    // $transactions = Transaction::with('card','submerchant')->where('swiped_mag_stripe','=',1)
    // ->get();
    // dd($transactions);
}
public function Collection_Balance($transaction){

    $issue="Collection Balance";
    // return view('layout.cardMethods')->with('transactions', $transactions);
    $checklist=[
        "Such transactions pose a risk and will be investigated"
    ];

   if($transaction->collection_balance > 0){

    $this->addToIncident($transaction->id,$issue,$checklist);
   }

}

}
