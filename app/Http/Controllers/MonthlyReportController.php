<?php

namespace App\Http\Controllers;
use App\Models\Checklist;
use App\Models\Incidents;
use App\Models\Match_table;
use App\Models\watchlist;
use Illuminate\Http\Request;
use App\models\DailyExceptionReport;
use App\Models\Transaction;
use App\Models\Submerchant;
use App\Models\Resolution;
use Auth;
use Carbon\Carbon;


class MonthlyReportController extends Controller
{
    public function index()
    {
        $MonthlyExceptionReports = Transaction::with('transactionType','submerchant')->get();
        $transactions = [];

        foreach($MonthlyExceptionReports as $transaction){

        }
       $incidents = Incidents::with('transaction.transactionType','transaction.submerchant','checklist')->get();
        // dd($incidents);
        return $incidents;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }
    public function getMatchList(){
      $matches =  Match_table::with('submerchant')->get();
      return $matches;
    }
    public function getWatchList(){
       $watches = watchlist::with('submerchant')->get();
        return $watches;
      }
    public function TopProcessingSubmerchants(){
        $topSubmerchants = [];
        $topCount = 2;

        $topSubMerchants[] = SubMerchant::select('submerchants.*')
            ->join('transactions', 'submerchants.id', '=', 'transactions.submerchant_id')
            ->selectRaw('SUM(transactions.amount) as total_amount')
            ->selectRaw('COUNT(*) as count')
            ->groupBy('submerchants.id')
            ->orderByDesc('total_amount')
            ->limit($topCount)
            ->get();


            $topSubMerchants[] = SubMerchant::select('submerchants.*')
            ->join('transactions', 'submerchants.id', '=', 'transactions.submerchant_id')
            ->where('transactions.transaction_type_id','=', 2)
            ->selectRaw('SUM(transactions.amount) as total_amount')
            ->selectRaw('COUNT(*) as count')
            ->groupBy('submerchants.id')
            ->orderByDesc('total_amount')
            ->limit($topCount)
            ->get();

            $topSubMerchants[] = SubMerchant::select('submerchants.*')
            ->join('transactions', 'submerchants.id', '=', 'transactions.submerchant_id')
            ->where('transactions.transaction_type_id','=', 3)
            ->selectRaw('SUM(transactions.amount) as total_amount')
            ->selectRaw('COUNT(*) as count')
            ->groupBy('submerchants.id')
            ->orderByDesc('total_amount')
            ->limit($topCount)
            ->get();



        return $topSubMerchants;


    }
    public function Restricted_Business_Type_Review(){
        dd(Submerchant::where('isActive','=',3)->get());


    }
    public function getVolume($submerchant,$start_time,$end_time){
       return Transaction::with('submerchant')
->where('submerchant_id', $submerchant->id)
    ->whereBetween('created_at', [$start_time, $end_time])
    ->sum('amount');
    }
    public function Decreasing_Volume_Submerchants(){
        $submerchants = Submerchant::all();
        foreach($submerchants as $submerchant){

            $submerchant->total = $this->getVolume($submerchant,Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth());
            $submerchant->previous_month = $this->getVolume($submerchant,Carbon::now()->subMonth()->startOfMonth(),Carbon::now()->subMonth()->endOfMonth());
            $submerchant->previous_two_month = $this->getVolume($submerchant,Carbon::now()->subMonths(2)->startOfMonth(),Carbon::now()->subMonths(2)->endOfMonth());
        }

        dd($submerchants);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
