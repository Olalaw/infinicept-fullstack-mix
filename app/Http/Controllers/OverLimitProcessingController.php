<?php

namespace App\Http\Controllers;
use App\Models\AdminThresholds;
use App\Models\Checklist;
use App\Models\OverLimitIncidents;
use Illuminate\Http\Request;
use App\models\DailyExceptionReport;
use App\Models\Transaction;
use App\Models\Submerchant;
use App\Models\Resolution;
use App\Services\MatchlistService;
use App\Models\Match_table;
use Auth;
use DB;
use Carbon\Carbon;

class OverLimitProcessingController extends Controller
{
    private $matchlistService;

    public function __construct(MatchlistService $matchlistService)
    {
        $this->matchlistService = $matchlistService;
    }
    public function index()
    {
        $currentMonth = Carbon::now()->startOfMonth();
        $overLimitReports =  Submerchant::all();
        foreach($overLimitReports as $submerchant){
            $this->Monitoring_and_or_Tracking_of_the_Monthly_Approved_Limit($submerchant);
            $this->Submerchant_Excessive_Processing_Review($submerchant);
            $this->Out_of_pattern($submerchant);
        }





    $incidents = OverLimitIncidents::with('submerchant','checklist')->get();
        // dd($incidents);
        return $incidents;
    // return view('daily_exception_reports.index', compact('dailyExceptionReports'));
}
public function getReport($id)
{
    $months =[];
    for($i=0;$i<6;$i++){
        $enddate = Carbon::now()->subMonthsNoOverflow($i)->endOfMonth();
        $startDate = $enddate->copy()->startOfMonth();
       $months[] = $this->matchlistService->getMonths($startDate,$enddate,$id,'monthly');
    }


    return($months);


}
public function getWeeklyReport($id)
{
    $weeks =[];
    for($i=0;$i<4;$i++){
       $weeks[] = $this->matchlistService->getMonths(Carbon::now()->subWeeks($i)->startOfWeek(),Carbon::now()->subWeeks($i)->endOfWeek(),$id,'weekly');
    }
    // dd($weeks);
    return($weeks);


}
// public function getMonths($start,$end,$id,$search){

// $newMonth =[];
// $total = Transaction::where('submerchant_id',$id)
//          ->whereBetween('created_at',[$start,$end])
//          ->where('transaction_type_id',1)
//          ->sum('amount');

// $average = Transaction::where('submerchant_id',$id)
//         ->whereBetween('created_at',[$start,$end])
//         ->where('transaction_type_id',1)
//          ->avg('amount');
// $count = Transaction::where('submerchant_id',$id)
//         ->whereBetween('created_at',[$start,$end])
//         ->where('transaction_type_id',1)
//          ->count();
// $refund = Transaction::where('submerchant_id',$id)
//          ->whereBetween('created_at',[$start,$end])
//          ->where('transaction_type_id',2)
//          ->count();
// $refundVolume = Transaction::where('submerchant_id',$id)
//         ->whereBetween('created_at',[$start,$end])
//          ->where('transaction_type_id',2)
//          ->sum('amount');
// $chargeback = Transaction::where('submerchant_id',$id)
//         ->whereBetween('created_at',[$start,$end])
//          ->where('transaction_type_id',3)
//          ->count();
// $chargebackVolume = Transaction::where('submerchant_id',$id)
//          ->whereBetween('created_at',[$start,$end])
//          ->where('transaction_type_id',3)
//          ->sum('amount');
// $chargeBackRatio = $this->getRatio($chargeback,$count);
// $refundRatio = $this->getRatio($refund,$count);

//     $newMonth['total'] = $total;
//     $newMonth['average'] = $average;
//     $newMonth['count'] = $count;
//     $newMonth['refund']= $refund;
//     $newMonth['refundVolume'] = $refundVolume;
//     $newMonth['chargeback'] = $chargeback;
//     $newMonth['chargebackVolume'] = $chargebackVolume;
//     $newMonth['chargeBackRatio'] = $chargeBackRatio;
//     $newMonth['refundRatio'] = $refundRatio;
//     $newMonth['startDate'] = $start;
//     $newMonth['endDate'] = $end;
//     if($search=='monthly'){
//     $AdminChargebackThreshold = AdminThresholds::where('id',1)->first()->chargeback_threshold;
//     $AdminRefundThreshold = AdminThresholds::where('id',1)->first()->refund_threshold;
//     $SubmerchantChargebackThreshold = Submerchant::where('id',$id)->first()->chargeback_threshold;
//     $SubmerchantRefundThreshold = Submerchant::where('id',$id)->first()->refund_threshold;
//     }
//     else{
//         $AdminChargebackThreshold = AdminThresholds::where('id',1)->first()->weekly_chargeback_threshold;

//         $AdminRefundThreshold = AdminThresholds::where('id',1)->first()->weekly_refund_threshold;
//         $SubmerchantChargebackThreshold = Submerchant::where('id',$id)->first()->weekly_chargeback_threshold;
//         $SubmerchantRefundThreshold = Submerchant::where('id',$id)->first()->weekly_refund_threshold;
//     }
//     if($this->checkThresholds($chargeBackRatio,$AdminChargebackThreshold)){
//         $newMonth['chargeback_status'] = "Danger";

//     }
//     else{
//         if($this->checkThresholds($chargeBackRatio,$SubmerchantChargebackThreshold)){
//             $newMonth['chargeback_status'] = "Warning";
//         }
//         else{
//             $newMonth['chargeback_status'] = 'Safe';
//         }
//     }
//     if($this->checkThresholds($refundRatio,$AdminRefundThreshold)){
//         $newMonth['refund_status'] = "Danger";
//         $this->addToMatch($id);
//     }
//     else{
//         if($this->checkThresholds($refundRatio,$SubmerchantRefundThreshold)){
//             $newMonth['refund_status'] = "Warning";
//         }
//         else{
//             $newMonth['refund_status'] = 'Safe';
//         }
//     }
//     // dd($newMonth);
//     return $newMonth;
// }
// public function checkThresholds($num1,$num2){
//     if($num1>$num2){
//         return true;
//     }
//     else{
//         return false;
//     }
// }
// public function getRatio($num1,$num2){
//     if($num1==0){
//         return 0;
//     }
//     if($num2==0){
//         return 100;
//     }
//     return ($num1/$num2)*100;

// }
// public function addToMatch($id){
//     $submerchant = Submerchant::where('id',$id)->first();
//     // dd($submerchant);
//     $match = Match_table::firstOrCreate(
//         [
//             'submerchant_id' => $id,

//         ],
//         [
//             'address' => $submerchant->address,
//             'phone' => $submerchant->phone,
//             'email' => $submerchant->email,
//             // Other attributes you want to set for a new record
//         ]
//     );
// }
public function addToIncident($id,$issue,$checklist,$volume,$count,$average){
    $incident = OverLimitIncidents::firstOrCreate(
        [
            'submerchant_id' => $id,
            'issue' => $issue,

        ],
        [
            'volume' => $volume,
            'count' => $count,
            'avergae' => $average,
            'status' => false,
            'image' => null,
            // Other attributes you want to set for a new record
        ]
    );

   foreach($checklist as $check){
    Checklist::firstOrcreate([
        'over_limit_incidents_id' => $incident->id,
        'name' => $check,
    ],

        [

        'status' => 0,

    ]);
}
}
    public function Monitoring_and_or_Tracking_of_the_Monthly_Approved_Limit($submerchant){
        {
            // $subMerchants = SubMerchant::all();
            // $subMerchantsExceedingLimit = [];
            $issue="Surpassed 90% of Threshold";
            // return view('layout.cardMethods')->with('transactions', $transactions);
            $checklist=[
                "The approved limit and gross sales for the current month should be used to project a month-end processing total.
                ",
                "If the projection indicates probable over-limit processing, submerchant contact may be warranted.  At that time, a temporary limit increase may be approved by Risk to accommodate the processing needs of the submerchant.
                ",

            ];
                $lastMonthStartDate = Carbon::now()->subMonth()->startOfMonth();
                $lastMonthEndDate = Carbon::now()->subMonth()->endOfMonth();

                $totalTransactions = Transaction::select(

                    DB::raw('SUM(amount) as total'),
                    DB::raw('COUNT(*) as trans_count'),
                    DB::raw('AVG(amount) as trans_avg')
                )
                ->where('submerchant_id', $submerchant->id)
                // ->whereBetween('created_at', [$lastMonthStartDate, $lastMonthEndDate])
                ->first();
                // dd($totalTransactions);
                $limitThreshold = $submerchant->monthly_processing_limit * 0.9; // 90% of the provided limit

                if ($totalTransactions->total > $limitThreshold) {

                    // Perform actions if the total transactions exceed the limit threshold
                    // For example, you can log the information, send notifications, etc.
                   $this->addToIncident($submerchant->id,
                   $issue,
                   $checklist,
                   $totalTransactions->total,
                   $totalTransactions->trans_count,
                   $totalTransactions->trans_avg);
                }

            // dd($totalTransactions);
        }
    }
    // public function Fallback_Transactions($transaction){

    // }
    public function Submerchant_Excessive_Processing_Review($submerchant){


        $issue="Excessive Processing";
        // return view('layout.cardMethods')->with('transactions', $transactions);
        $checklist=[
          "If the conditions below are met per Section 7.2 of RapidCents’s Underwriting Policy, a review for proper graduation of the submerchant from the RapidCents program and into a tri-party agreement with Elavon is warranted."

        ];
        $lastMonthStartDate = Carbon::now()->startOfMonth();
        $lastYearStartDate = Carbon::now()->subYear()->startOfMonth();
        $lastMonthEndDate = Carbon::now();

        $totalTransactions = Transaction::select(

            DB::raw('SUM(amount) as total'),
            DB::raw('COUNT(*) as trans_count'),
            DB::raw('AVG(amount) as trans_avg'),
            DB::raw('SUM(amount)/12 as trans_month_avg')
        )
        ->where('submerchant_id', $submerchant->id)
        ->whereBetween('created_at', [$lastYearStartDate, $lastMonthEndDate])
        ->first();
        if ($totalTransactions->total > 10000.23) { //remember to change this value


            // Perform actions if the total transactions exceed the limit threshold
            // For example, you can log the information, send notifications, etc.
           $this->addToIncident($submerchant->id,
           $issue,
           $checklist,
           $totalTransactions->total,
           $totalTransactions->trans_count,
           $totalTransactions->trans_avg);
        //    return;
        }
        if($totalTransactions->trans_month_avg > 1053.00){ //remember to change this value

            $this->addToIncident($submerchant->id,
            $issue,
            $checklist,
            $totalTransactions->total,
            $totalTransactions->trans_count,
            $totalTransactions->trans_avg);
            // return;
        }
        else{

            $totalMonth = Transaction::select(

                DB::raw('SUM(amount) as total'),
                DB::raw('COUNT(*) as trans_count'),
                DB::raw('AVG(amount) as trans_avg'),

            )
            ->where('submerchant_id', $submerchant->id)
            ->whereBetween('created_at', [$lastMonthStartDate, $lastMonthEndDate])
            ->first();

            if($totalMonth->total > 2000.00){ //remember to change this value
                // dd($totalMonth->total);
                $this->addToIncident($submerchant->id,
                $issue,
                $checklist,
                $totalTransactions->total,
                $totalTransactions->trans_count,
                $totalTransactions->trans_avg);
                return;
            }

        }
        // foreach ($subMerchants as $subMerchant) {
            // $lastYearStartDate = Carbon::now()->subYear();
            // $lastYearEndDate = Carbon::now();

            // $lastMonthStartDate = Carbon::now()->subMonth()->startOfMonth();
            // $lastMonthEndDate = Carbon::now()->subMonth()->endOfMonth();

            // $totalYearTransactions = Transaction::where('submerchant_id', $subMerchant->id)
            //     ->whereBetween('created_at', [$lastYearStartDate, $lastYearEndDate])
            //     ->sum('amount');

            //     $totalMonthTransactions = SubMerchant::select('submerchants.*')
            // ->join('transactions', 'submerchants.id', '=', 'transactions.submerchant_id')
            // ->selectRaw('YEAR(transactions.created_at) as year')
            // ->selectRaw('SUM(transactions.amount) as total_amount')
            // ->selectRaw('COUNT(DISTINCT MONTH(transactions.created_at)) as total_months')
            // ->whereYear('transactions.created_at', '=', 2023)
            // ->groupBy('submerchants.id', 'year')
            // ->havingRaw('(SUM(transactions.amount) / COUNT(DISTINCT MONTH(transactions.created_at))) > ?', [106000])
            // ->get();

            // // 90% of the provided limit
            // $totalAnyMonth = SubMerchant::select('submerchants.*')
            // ->join('transactions', 'submerchants.id', '=', 'transactions.submerchant_id')
            // ->selectRaw('YEAR(transactions.created_at) as year, MONTH(transactions.created_at) as month')
            // ->selectRaw('SUM(transactions.amount) as total_amount')
            // ->whereYear('transactions.created_at', '=', now()->year)
            // ->groupBy('submerchants.id', 'year', 'month')
            // ->havingRaw('SUM(transactions.amount) > ?', [500000])
            // ->get();
            // if ($totalYearTransactions > 1000000.00) {
            //     // Perform actions if the total transactions exceed the limit threshold
            //     // For example, you can log the information, send notifications, etc.
            //     $subMerchantsExceedingYear[] = $subMerchant;
            // }


        // }
        // $subMerchantsExceedingLimit =[$subMerchantsExceedingYear,$totalAnyMonth,$totalMonthTransactions];
        // dd($subMerchantsExceedingLimit);
    }
    public function Out_of_pattern ($submerchant){

            $issue="Weekly Volume of over 6300 CAD";
            $checklist=[
               "Has it been monitored for out-of- pattern activity that represents 50% variance (greater or lesser) compared to the approved account "

            ];
                $lastWeekStartDate = Carbon::now()->startOfWeek();

                $totalTransactions = Transaction::select(

                    DB::raw('SUM(amount) as total'),
                    DB::raw('COUNT(*) as trans_count'),
                    DB::raw('AVG(amount) as trans_avg')
                )
                ->where('submerchant_id', $submerchant->id)
                ->whereBetween('created_at', [$lastWeekStartDate, Carbon::now()])
                ->first();
                if($totalTransactions->total>6300.00){

                $this->addToIncident($submerchant->id,
                $issue,
                $checklist,
                $totalTransactions->total,
                $totalTransactions->trans_count,
                $totalTransactions->trans_avg);

                return;
                }
    }

}
