<?php
// App\Services\WatchlistService.php

namespace App\Services;

use App\Models\SubMerchant;
use App\Models\Match_table;
use App\Models\AdminThresholds;
USE App\Models\Transaction;
use App\Models\watchlist;
use Carbon\Carbon;

class MatchlistService
{
    public function runChecks(SubMerchant $subMerchant)
    {
        // Logic to add sub-merchant to the watch list
        // For example: $subMerchant->update(['is_on_watchlist' => true]);
    }
    public function addToMatch($id){
        $submerchant = Submerchant::where('id',$id)->first();
        // dd($submerchant);
        $match = Match_table::firstOrCreate(
            [
                'submerchant_id' => $id,

            ],
            [
                'address' => $submerchant->address,
                'phone' => $submerchant->phone,
                'email' => $submerchant->email,
                // Other attributes you want to set for a new record
            ]
        );
    }
    public function addToWatch($id){
        $submerchant = Submerchant::where('id',$id)->first();
        // dd($submerchant);
        $match = watchlist::firstOrCreate(
            [
                'submerchant_id' => $id,

            ],
            [
                'address' => $submerchant->address,
                'phone' => $submerchant->phone,
                'email' => $submerchant->email,
                // Other attributes you want to set for a new record
            ]
        );
    }
    public function checkThresholds($num1,$num2){
        if($num1>$num2){
            return true;
        }
        else{
            return false;
        }
    }
    public function getRatio($num1,$num2){
        if($num1==0){
            return 0;
        }
        if($num2==0){
            return 100;
        }
        return ceil(($num1 / $num2) * 100);

    }
    public function getMonths($start,$end,$id,$search){

        $newMonth =[];
        $total = Transaction::where('submerchant_id',$id)
                 ->whereBetween('created_at',[$start,$end])
                 ->where('transaction_type_id',1)
                 ->sum('amount');

        $average = Transaction::where('submerchant_id',$id)
                ->whereBetween('created_at',[$start,$end])
                ->where('transaction_type_id',1)
                 ->avg('amount');
        $count = Transaction::where('submerchant_id',$id)
                ->whereBetween('created_at',[$start,$end])
                ->where('transaction_type_id',1)
                 ->count();
        $refund = Transaction::where('submerchant_id',$id)
                 ->whereBetween('created_at',[$start,$end])
                 ->where('transaction_type_id',2)
                 ->count();
        $refundVolume = Transaction::where('submerchant_id',$id)
                ->whereBetween('created_at',[$start,$end])
                 ->where('transaction_type_id',2)
                 ->sum('amount');
        $chargeback = Transaction::where('submerchant_id',$id)
                ->whereBetween('created_at',[$start,$end])
                 ->where('transaction_type_id',3)
                 ->count();
        $chargebackVolume = Transaction::where('submerchant_id',$id)
                 ->whereBetween('created_at',[$start,$end])
                 ->where('transaction_type_id',3)
                 ->sum('amount');
        $chargeBackRatio = $this->getRatio($chargeback,$count);
        $refundRatio = $this->getRatio($refund,$count);

            $newMonth['total'] = $total;
            $newMonth['average'] = $average;
            $newMonth['count'] = $count;
            $newMonth['refund']= $refund;
            $newMonth['refundVolume'] = $refundVolume;
            $newMonth['chargeback'] = $chargeback;
            $newMonth['chargebackVolume'] = $chargebackVolume;
            $newMonth['chargeBackRatio'] = $chargeBackRatio;
            $newMonth['refundRatio'] = $refundRatio;
            $newMonth['startDate'] = $start;
            $newMonth['endDate'] = $end;
            if($search=='monthly'){
            $AdminChargebackThreshold = AdminThresholds::where('id',1)->first()->chargeback_threshold;
            $AdminRefundThreshold = AdminThresholds::where('id',1)->first()->refund_threshold;
            $SubmerchantChargebackThreshold = Submerchant::where('id',$id)->first()->chargeback_threshold;
            $SubmerchantRefundThreshold = Submerchant::where('id',$id)->first()->refund_threshold;
            }
            else{
                $AdminChargebackThreshold = AdminThresholds::where('id',1)->first()->weekly_chargeback_threshold;

                $AdminRefundThreshold = AdminThresholds::where('id',1)->first()->weekly_refund_threshold;
                $SubmerchantChargebackThreshold = Submerchant::where('id',$id)->first()->weekly_chargeback_threshold;
                $SubmerchantRefundThreshold = Submerchant::where('id',$id)->first()->weekly_refund_threshold;
            }
            if($this->checkThresholds($chargeBackRatio,$AdminChargebackThreshold)){
                $newMonth['chargeback_status'] = "Danger";
                if($start== Carbon::now()->subMonth()->startOfMonth()){
                $this->addToMatch($id);
            }

            }
            else{
                if($this->checkThresholds($chargeBackRatio,$SubmerchantChargebackThreshold)){
                    $newMonth['chargeback_status'] = "Warning";
                    if($start== Carbon::now()->subMonth()->startOfMonth()){
                        $this->addToWatch($id);
                    }
                }
                else{
                    $newMonth['chargeback_status'] = 'Safe';
                }
            }
            if($this->checkThresholds($refundRatio,$AdminRefundThreshold)){
                $newMonth['refund_status'] = "Danger";
                if($start== Carbon::now()->subMonth()->startOfMonth()){
                    $this->addToMatch($id);
                }
            }
            else{
                if($this->checkThresholds($refundRatio,$SubmerchantRefundThreshold)){
                    $newMonth['refund_status'] = "Warning";
                    if($start== Carbon::now()->subMonth()->startOfMonth()){
                        $this->addToWatch($id);
                    }
                }
                else{
                    $newMonth['refund_status'] = 'Safe';
                }
            }
            // dd($newMonth);
            return $newMonth;
        }
}
