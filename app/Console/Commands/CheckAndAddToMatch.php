<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SubMerchant;
use App\Services\MatchlistService;
use Carbon\Carbon;

class CheckAndAddToMatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    private $matchlistService;

    public function __construct(MatchlistService $matchlistService)
    {
        parent::__construct();
        $this->matchlistService = $matchlistService;
    }
     protected $signature = 'app:matchlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Submerchants and add high risk to MATCH Db';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        //
        $subMerchants = SubMerchant::all();
        foreach ($subMerchants as $subMerchant) {
            $enddate = Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
            $startDate = $enddate->copy()->startOfMonth();
            // Use the WatchlistService to add sub-merchant to the watch list
            $this->matchlistService->getMonths($startDate,$enddate,$subMerchant->id,'monthly');
        }
        $this->info('Matchlist check completed.');
    }
}
